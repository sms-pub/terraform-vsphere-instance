##################################################################################
# VERSIONS
##################################################################################

terraform {
  required_providers {
    vsphere = {
      source  = "hashicorp/vsphere"
      version = ">= 2.2.0"
    }
  }
  required_version = ">= 1.2.5"
}

##################################################################################
# DATA
##################################################################################

data "vsphere_datacenter" "datacenter" {
  name = var.vsphere_datacenter
}

##################################################################################
# RESOURCES
##################################################################################

resource "vsphere_role" "packer_vsphere" {
  name            = var.packer_vsphere_role
  role_privileges = var.packer_vsphere_privileges
}
