##################################################################################
# VARIABLES
##################################################################################

# vSphere Settings
variable "vsphere_datacenter" {
  type        = string
  description = "The target vSphere datacenter object name. (e.g. sfo-m01-dc01)."
}

# Roles
variable "packer_vsphere_role" {
  type        = string
  description = "The name for the HashiCorp Packer to VMware vSphere custom role."
  default     = "Packer to vSphere Integration"
}

variable "packer_vsphere_privileges" {
  type        = list(string)
  description = "The vSphere privledges for the HashiCorp Packer to VMware vSphere custom role."
  default = [
    # Example:
    #
    # "Alarm.Acknowledge",
    # "Alarm.Create",
    # "Datacenter.Create",
    # "Datacenter.Move"
  ]
}
