output "vm_id" {
  description = "The UUID of the virtual machine."
  value       = vsphere_virtual_machine.vm.id
}

output "vm_moid" {
  description = "The managed object reference ID of the created virtual machine."
  value       = vsphere_virtual_machine.vm.moid
}

output "vm_tools_status" {
  description = "The state of VMware Tools in the guest."
  value       = vsphere_virtual_machine.vm.vmware_tools_status
}

output "vm_vmx_path" {
  description = "The path of the virtual machine configuration file on the datastore in which the virtual machine is placed."
  value       = vsphere_virtual_machine.vm.vmx_path
}

output "vm_ip_address" {
  description = "The IP address selected by Terraform to be used with any provisioners configured on this resource."
  value       = vsphere_virtual_machine.vm.default_ip_address
}

output "vm_ip_addresses" {
  description = "The current list of IP addresses on this machine, including the value of default_ip_address."
  value       = vsphere_virtual_machine.vm.guest_ip_addresses
}
