terraform {
  required_providers {
    vsphere = {
      source  = "hashicorp/vsphere"
      version = ">= 2.2.0"
    }
  }
  required_version = ">= 1.2.5"
}

provider "vsphere" {
  vsphere_server       = var.vsphere_server
  user                 = var.vsphere_username
  password             = var.vsphere_password
  allow_unverified_ssl = var.vsphere_insecure
}

module "role" {
  source = "../../modules/vsphere-role"

  vsphere_datacenter        = var.vsphere_datacenter
  packer_vsphere_privileges = var.packer_vsphere_privileges
}
