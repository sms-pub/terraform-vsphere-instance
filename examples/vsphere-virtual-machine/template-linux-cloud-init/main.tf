terraform {
  required_providers {
    vsphere = {
      source  = "hashicorp/vsphere"
      version = ">= 2.2.0"
    }
  }
  required_version = ">= 1.2.5"
}

provider "vsphere" {
  vsphere_server       = var.vsphere_server
  user                 = var.vsphere_username
  password             = var.vsphere_password
  allow_unverified_ssl = var.vsphere_insecure
}

module "instance" {
  source = "../../../modules/vsphere-virtual-machine/template-linux-cloud-init"

  # vSphere Settings
  vsphere_datacenter = var.vsphere_datacenter
  vsphere_cluster    = var.vsphere_cluster
  vsphere_folder     = var.vsphere_folder
  vsphere_datastore  = var.vsphere_datastore
  vsphere_network    = var.vsphere_network
  vsphere_template   = var.vsphere_template

  # Virtual Machines Settings
  vm_name                    = var.vm_name
  vm_cpus                    = var.vm_cpus
  vm_memory                  = var.vm_memory
  vm_disk_size               = var.vm_disk_size
  vm_efi_secure_boot_enabled = var.vm_efi_secure_boot_enabled

  guestinfo_metadata = base64encode(templatefile("${path.module}/metadata.yml.tftpl", {
    vm_name = var.vm_name
  }))

  guestinfo_userdata = base64encode(templatefile("${path.module}/userdata.yml.tftpl", {
    user_name          = var.user_name
    user_passwd        = var.user_passwd
    ssh_authorized_key = var.ssh_authorized_key
    package_list       = var.package_list
  }))
}
