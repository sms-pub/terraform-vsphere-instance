terraform {
  required_providers {
    vsphere = {
      source  = "hashicorp/vsphere"
      version = ">= 2.2.0"
    }
  }
  required_version = ">= 1.2.5"
}

provider "vsphere" {
  vsphere_server       = var.vsphere_server
  user                 = var.vsphere_username
  password             = var.vsphere_password
  allow_unverified_ssl = var.vsphere_insecure
}

module "instance" {
  source = "../../../modules/vsphere-virtual-machine/content-library-ovf-windows-guest-customization"

  # vSphere Settings
  vsphere_datacenter          = var.vsphere_datacenter
  vsphere_cluster             = var.vsphere_cluster
  vsphere_datastore           = var.vsphere_datastore
  vsphere_folder              = var.vsphere_folder
  vsphere_network             = var.vsphere_network
  vsphere_content_library     = var.vsphere_content_library
  vsphere_content_library_ovf = var.vsphere_content_library_ovf

  # Virtual Machines Settings
  vm_name                    = var.vm_name
  vm_cpus                    = var.vm_cpus
  vm_memory                  = var.vm_memory
  vm_disk_size               = var.vm_disk_size
  vm_firmware                = var.vm_firmware
  vm_efi_secure_boot_enabled = var.vm_efi_secure_boot_enabled
  vm_ipv4_address            = var.vm_ipv4_address
  vm_ipv4_netmask            = var.vm_ipv4_netmask
  vm_ipv4_gateway            = var.vm_ipv4_gateway
  vm_dns_suffix_list         = var.vm_dns_suffix_list
  vm_dns_server_list         = var.vm_dns_server_list
  domain                     = var.domain
  domain_admin_username      = var.domain_admin_username
  domain_admin_password      = var.domain_admin_password
  vm_admin_password          = var.vm_admin_password
}
